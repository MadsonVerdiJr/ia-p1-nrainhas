# v1.0.0

O programa deve ler um número inteiro _N_, encontrar uma solução para um tabuleiro _N_x_N_ com _N_ rainhas, e então imprimí-la na saída com o seguinte formato: _N_ números inteiros separados por espaço, onde o primeiro número corresponde à primeira coluna da esquerda, o _N_-ésimo número à coluna mais da direita, e cada número indica a linha (de cima para baixo) em que uma rainha está.

Exemplo:
Entrada: 4
Saída: 3 1 4 2
Que corresponde a esta posição:

'''

|   | X |   |   |
|   |   |   | X |
| X |   |   |   |
|   |   | X |   |

'''

*Data de entrega: 24/04*

Critérios: solução correta; ao menos 30 rainhas; código legível e elegante. *IMPORTANTE!*: há muito código na internet para este problema, o que signifca que é fácil eu descobrir se alguém copiou (também vou usar uma ferramenta de detecção de plágio). Então resistam à tentação de procurar a resposta. Lembrando que o plano de ensino prevê reprovação automática em caso de plágio.


