#!/usr/bin/python

from nQueensProblem import *
from sys import stdout, argv


def main(argv):
    if len(argv) != 1:
        sys.exit(2)

    problem = Problem(int(argv[0]))

    for position in problem.solve():
        # print(position.col)
        stdout.write(str(position.col) + ' ')

    stdout.write('\n')


if __name__ == "__main__":
    main(argv[1:])
