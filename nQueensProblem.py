class Problem(object):
    """Problem resolution"""
    def __init__(self, size):
        super(Problem, self).__init__()
        self.size = size

    def solve(self):
        # Used for initialize de problem
        positions = []
        for x in range(0, self.size):
            positions.append(Position())

        hasSolution = self.backtrack(0, positions)

        if hasSolution:
            return positions
        else:
            return [Position()]

        # solve

    def backtrack(self, row, positions):
        # Backtrack implementation
        # print row, (self.size == row)
        if self.size == row:
            return True

        for col in range(0, self.size):
            isSafe = True

            for queen in range(0, row):
                if positions[queen].checkAttack(row, col):
                    isSafe = False
                    break

            if isSafe:
                positions[row].setCell(row, col)
                if self.backtrack(row + 1, positions):
                    return True

        return False
        # backtrack


class Position(object):
    """Position on board"""
    def __init__(self):
        super(Position, self).__init__()
        self.row = None
        self.col = None
        self.cell = None

    def setRow(self, row):
        self.row = row
        if self.col is not None:
            self.cell = [self.row, self.col]

    def setCol(self, col):
        self.col = col
        if self.row is not None:
            self.cell = [self.row, self.col]

    def setCell(self, row, col):
        self.col = col
        self.row = row
        self.cell = [self.row, self.col]

    def checkAttack(self, row, col):
        return self.col == col or (self.row - self.col) == (row - col) or (self.row + self.col) == (row + col)
